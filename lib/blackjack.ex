defmodule Blackjack do

  @moduledoc """ 
  The entry point for the Blackjack game
  """

  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Define workers and child supervisors to be supervised
      supervisor(Blackjack.TableServer, []),
      supervisor(Blackjack.PlayerServer, []),
      supervisor(Blackjack.GameServer, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Blackjack.Server]
    Supervisor.start_link(children, opts)
  end
end
