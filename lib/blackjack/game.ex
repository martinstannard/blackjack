defmodule Blackjack.Game do

  @moduledoc """
  Blackjack.Game is a gen_fsm server, using Player and Table to provide
  an implementation of the game of Blackjack.

  The state machine provides states for:
  start_hand
  player_move
  dealer
  tally
  """

  alias Blackjack.Table
  alias Blackjack.Player

  # Client api
  def start_link(name) do
    :gen_fsm.start_link({:local, :"#{name}"}, __MODULE__, name, [])
  end

  def get_state(pid) do
    :gen_fsm.sync_send_event(pid, :get_state)
  end

  def deal(pid) do
    :gen_fsm.sync_send_event(pid, :deal)
  end

  def move(pid) do
    input = IO.gets("(H)it/(S)tand?")
    :gen_fsm.sync_send_event(pid, {:move, input})
  end

  def draw(pid) do
    :gen_fsm.sync_send_event(pid, :draw)
  end

  def name(pid) do
    :gen_fsm.sync_send_event(pid, :name)
  end

  def next(pid) do
    :gen_fsm.sync_send_event(pid, :next)
  end

  # Server api
  def init(name) do
    {:ok, table} = Blackjack.TableServer.add_table(name)
    {:ok, player} = Blackjack.PlayerServer.add_player(name)
    Table.add_player(table, player)
    game_state = %{table: table, player: player, name: name}
    {:ok, :start_hand, game_state}
  end
    
  def start_hand(:get_state, _from, game_state), do: {:reply, :start_hand, :start_hand, game_state}
  def start_hand({:move, _}, _from, game_state), do: {:reply, :start_hand, :start_hand, game_state}
  def start_hand(:next, _from, game_state), do: {:reply, :start_hand, :start_hand, game_state}
  def start_hand(:deal, _from, game_state) do
    Table.deal(game_state.table)
    Table.bids(game_state.table)
    {:reply, :ok, :player_move, game_state}
    #{:next_state, :player_move, game_state}
  end 

  def player_move(:get_state, _from, game_state), do: {:reply, :player_move, :player_move, game_state}
  def player_move(:deal, _from, game_state), do: {:reply, :player_move, :player_move, game_state}
  def player_move(:next, _from, game_state), do: {:reply, :player_move, :player_move, game_state}
  def player_move({:move, input}, _from, game_state) do
    r = cond do
      input == "h\n" -> :hit
      true -> :stand
      #m = Blackjack.Player.move(game_state.player)
    end
    move_reply(r, game_state)
  end 

  defp move_reply(current_move, game_state) do
    case current_move do
      :hit -> 
        bust = Table.hit_player(game_state.table)
        if bust do
          {:reply, :bust, :dealer, game_state}
        else
          {:reply, :hit, :player_move, game_state}
        end
      :stand -> {:reply, :stand, :dealer, game_state}
    end
  end

  def dealer(:get_state, _from, game_state), do: {:reply, :dealer, :dealer, game_state}
  def dealer({:move, _}, _from, game_state), do: {:reply, :dealer, :dealer, game_state}
  def dealer(:deal, _from, game_state), do: {:reply, :dealer, :dealer, game_state}
  def dealer(:next, _from, game_state), do: {:reply, :dealer, :dealer, game_state}
  def dealer(:draw, _from, game_state) do
    Table.hit_dealer(game_state.table)
    {:reply, :tally, :tally, game_state}
  end

  def tally(:get_state, _from, game_state), do: {:reply, :tally, :tally, game_state}
  def tally({:move, _}, _from, game_state), do: {:reply, :tally, :tally, game_state}
  def tally(:draw, _from, game_state), do: {:reply, :tally, :tally, game_state}
  def tally(:next, _from, game_state) do
    {:reply, :start_hand, :start_hand, game_state}
  end

end
