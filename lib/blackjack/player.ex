defmodule Blackjack.Player do

  @moduledoc """
  A player has a Hand
  """

  use GenServer

  alias Blackjack.Hand
  
  def start_link(player_name) do
    GenServer.start_link(__MODULE__, player_name)
  end

  def hand(pid) do
    GenServer.call(pid, :hand)
  end

  def name(pid) do
    GenServer.call(pid, :name)
  end

  def playing(pid) do
    GenServer.call(pid, :playing)
  end

  def move(pid) do
    GenServer.call(pid, :move)
  end

  def print(pid) do
    GenServer.call(pid, :print)
  end

  def bust?(pid) do
    GenServer.call(pid, :bust?)
  end

  def highest_score(pid) do
    GenServer.call(pid, :highest_score)
  end

  def play(pid) do
    GenServer.cast(pid, :play)
  end

  def leave(pid) do
    GenServer.cast(pid, :leave)
  end

  def draw(pid, card) do
    GenServer.cast(pid, %{draw: card})
  end

  def clear(pid) do
    GenServer.cast(pid, :clear)
  end

  def credits(pid) do
    GenServer.call(pid, :credits)
  end

  def transact(pid, amount) do
    GenServer.cast(pid, {:transact, amount})
  end

  ###
  # Server
  ###

  def init(player_name) do
    {:ok, h} = Hand.start_link
    {:ok, %{hand: h, name: player_name, playing: false, credits: 0}}
  end

  def handle_call(:hand, _from, state) do
    {:reply, state.hand, state}
  end

  def handle_call(:credits, _from, state) do
    {:reply, state.credits, state}
  end

  def handle_call(:name, _from, state) do
    {:reply, state.name, state}
  end

  def handle_call(:playing, _from, state) do
    {:reply, state.playing, state}
  end

  def handle_call(:move, _from, state) do
    scores = Hand.scores(state.hand)
    outcome = cond do
      Enum.min(scores) > 21 -> :bust
      Enum.min(scores) >= 20 -> :stand
      Enum.min(scores) < 16 -> :hit
      true -> :stand
    end
    {:reply, outcome, state}
  end

  def handle_call(:print, _from, state) do
    IO.puts String.ljust(state.name, 20) <> " " <> Hand.to_string(state.hand) 
    {:reply, state, state}
  end

  def handle_call(:bust?, _from, state) do
    {:reply, Hand.bust?(state.hand), state}
  end

  def handle_call(:highest_score, _from, state) do
    {:reply, Hand.highest_score(state.hand), state}
  end

  def handle_cast(%{draw: card}, state) do
    Hand.draw(state.hand, card)
    {:noreply, state}
  end

  def handle_cast({:transact, amount}, state) do
    {:noreply, Map.put(state, :credits, state.credits + amount)}
  end

  def handle_cast(:clear, state) do
    Hand.clear(state.hand)
    {:noreply, state}
  end

  def handle_cast(:play, state) do
    {:noreply, Map.put(state, :playing, true)}
  end

  def handle_cast(:leave, state) do
    {:noreply, Map.put(state, :playing, false)}
  end

end

