defmodule Blackjack.GameServer do

  @moduledoc """
  A supervisor for Blackjack.Game
  """
  use Supervisor

  def add_game(name) do
    Supervisor.start_child(__MODULE__, [name])
  end

  def find_game(name) do
    Enum.find games, fn(child) ->
      Blackjack.Game.name(child) == name
    end
  end

  def game_count do
    Supervisor.count_children(__MODULE__).active
  end

  def delete_game(game) do
    Supervisor.terminate_child(__MODULE__, game)
  end

  def games do
    __MODULE__
    |> Supervisor.which_children
    |> Enum.map(fn({_, child, _, _}) -> child end)
  end

  ###
  # Supervisor API
  ###

  def start_link do
    Supervisor.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    children = [
      worker(Blackjack.Game, [], restart: :transient)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end

end



