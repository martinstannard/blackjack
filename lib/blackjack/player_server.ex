defmodule Blackjack.PlayerServer do

  @moduledoc """
  A supervisor for players
  """

  use Supervisor
  alias Blackjack.Player

  def add_player(name) do
    Supervisor.start_child(__MODULE__, [name])
  end

  def delete_player(player) do
    Supervisor.terminate_child(__MODULE__, player)
  end

  def find_player(name) do
    Enum.find players, fn(child) ->
      Player.name(child) == name
    end
  end

  def find_available_player do
    Enum.find players, fn(child) ->
      Player.playing(child) == false
    end
  end

  def players do
    __MODULE__
    |> Supervisor.which_children
    |> Enum.map(fn({_, child, _, _}) -> child end)
  end

  ###
  # Supervisor API
  ###

  def start_link do
    Supervisor.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    children = [
      worker(Player, [], restart: :transient)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end

end


