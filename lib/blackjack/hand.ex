defmodule Blackjack.Hand do

  @moduledoc """
  A hand of cards
  """
  
  use GenServer

  alias Blackjack.Counter

  def start_link do 
    GenServer.start_link(__MODULE__, nil)
  end

  def count(pid) do
    GenServer.call(pid, :count)
  end

  def scores(pid) do
    GenServer.call(pid, :scores)
  end

  def to_string(pid) do
    GenServer.call(pid, :to_string)
  end

  def bust?(pid) do
    GenServer.call(pid, :bust?)
  end

  def highest_score(pid) do
    GenServer.call(pid, :highest_score)
  end

  def draw(pid, card) do
    GenServer.cast(pid, {:draw, card})
  end

  def clear(pid) do
    GenServer.cast(pid, :clear)
  end

  ###
  # Server
  ###

  def init(_) do
    {:ok, []}
  end

  def handle_call(:count, _from, cards) do
    {:reply, length(cards), cards}
  end

  def handle_call(:scores, _from, cards) do
    {:reply, Counter.scores(cards), cards}
  end

  def handle_call(:to_string, _from, cards) do
    s = cards
    |> Enum.map(fn(c) -> elem(c, 0) end)
    |> Enum.reverse
    |> Enum.join("-")
    |> String.ljust(20)
    {:reply, s <> " [" <> Counter.best_score(cards) <> "]", cards}
  end

  def handle_call(:bust?, _from, cards) do
    {:reply, Counter.best_score(cards) == "bust", cards}
  end

  def handle_call(:highest_score, _from, cards) do
    {:reply, Counter.highest_score(cards), cards}
  end

  def handle_cast({:draw, card}, cards) do
    {:noreply, [card|cards]}
  end

  def handle_cast(:clear, _) do
    {:noreply, []}
  end

end
