defmodule Blackjack.Table do

  @moduledoc """
  A Table is created when a Player joins the game.
  The table contains 
  """

  use GenServer

  alias Blackjack.Deck
  alias Blackjack.Player

  def start_link(name) do
    GenServer.start_link(__MODULE__, name, name: :"table_#{name}")
  end

  def add_player(pid, player) do
    GenServer.call(pid, {:add_player, player})
  end

  def deal(pid) do
    GenServer.call(pid, :deal)
  end

  def bids(pid) do
    GenServer.call(pid, :bids)
  end

  def name(pid) do
    GenServer.call(pid, :name)
  end

  def hit_player(pid) do
    GenServer.call(pid, :hit_player)
  end

  def hit_dealer(pid) do
    GenServer.call(pid, :hit_dealer)
  end

  def init(name) do
    {:ok, deck} = Deck.start_link
    {:ok, 
      %{
        deck: deck, 
        name: "table_#{name}", 
        dealer: create_player("#{name}_dealer"), 
        opponents: create_opponents(name),
        player: nil
      }
    }
  end

  def handle_call({:add_player, player}, _from, state) do
    Player.play(player)
    {:reply, player, Map.put(state, :player, player)}
  end

  def handle_call(:deal, _from, state) do
    clear_hands(state)
    deal_hands(state)
    IO.puts "INITIAL DEAL"
    print state
    {:reply, :ok, state}
  end

  def handle_call(:name, _from, state) do
    {:reply, state[:name], state}
  end

  def handle_call(:bids, _from, state) do
    take_bids(state.opponents, state.deck)
    IO.puts "OPPONENTS DRAWING..."
    print(state)
    {:reply, :ok, state}
  end

  def handle_call(:hit_player, _from, state) do
    Player.draw(state.player, Deck.hit(state.deck))
    print(state)
    {:reply, Player.bust?(state.player), state}
  end

  def handle_call(:hit_dealer, _from, state) do
    dealer_draw(state)
    IO.puts "DEALER HAS DRAWN"
    print(state)
    IO.puts "THE END"
    {:reply, :ok, state}
  end

  defp dealer_draw(state) do
    if Player.highest_score(state.dealer) < 17 do
      Player.draw(state.dealer, Deck.hit(state.deck))
      dealer_draw(state)
    end
  end

  defp create_player(player_name) do
    {:ok, player} = Player.start_link(player_name)
    player
  end

  defp take_bids([], _), do: :ok
  defp take_bids([h|t], deck) do
    ask_player(h, deck) 
    take_bids(t, deck)
  end

  defp ask_player(player, deck) do
    case Player.move(player) do
      :hit -> 
        Player.draw(player, Deck.hit(deck))
        ask_player(player, deck)
      :bust -> :ok
      :stand -> :ok
    end
  end

  defp print(state) do
    state.opponents
    |> Enum.each(&Player.print(&1))
    Player.print(state.player)
    Player.print(state.dealer)
  end

  defp clear_hands(state) do
    state.opponents
    |> Enum.each(&Player.clear(&1))
    Player.clear(state.player)
    Player.clear(state.dealer)
  end

  defp deal_hands(state) do
    state.opponents
    |> Enum.each(&deal_to_player(&1, state.deck))
    deal_to_player(state.player, state.deck)
    deal_to_player(state.dealer, state.deck)
  end

  defp deal_to_player(nil, _), do: nil
  defp deal_to_player(player, deck) do
    Player.draw(player, Deck.hit(deck))
    Player.draw(player, Deck.hit(deck))
  end

  defp create_opponents(name) do
    (1..4)
    |> Enum.map(&create_player("#{name}_op_#{&1}"))
  end

end
