# Blackjack

A blackjack application in Elixir

## Architecture

All below is subject to change as I learn more about Applications and Supervisors.

The application uses 3 Supervisors to manage the main game modules.

* PlayerSupervisor manages Player. These are the external users that will play games
against the app or each other.
* TableSupervisor manages Tables. A Table has a Deck, a dealer, a number of app-controlled
opponents, and a Player.
* GameSupervisor manages Games. A Game is a gen_fsm state machine that handles game state.

![Blackjack Application](/blackjack.png)

## Usage
A Game has these states:

* `start_hand` -> call `Game.deal(game)` to deal the first round of cards, and have opponents
draw or stand
* `player_move` -> call `Game.move(game)` to make a move. You will be prompted to Hit or Stand.
Press `h` to hit, anything else to stand.
* `dealer` -> call `Game.draw(game)` for the dealer to draw his cards.
* `tally` -> the hand is finished, there is not scoring yet... call `Game.next(game)` for the
next hand

## Example

```
Interactive Elixir (1.2.0) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> {:ok, g} = Blackjack.GameServer.add_game("brian")
{:ok, #PID<0.154.0>}
iex(2)> Blackjack.Game.deal(g)                           
INITIAL DEAL
brian_op_1           A-2                  [13]
brian_op_2           6-5                  [11]
brian_op_3           2-9                  [11]
brian_op_4           7-Q                  [17]
brian                5-10                 [15]
brian_dealer         9-8                  [17]
OPPONENTS DRAWING...
brian_op_1           A-2-7-7              [17]
brian_op_2           6-5-9                [20]
brian_op_3           2-9-8                [19]
brian_op_4           7-Q                  [17]
brian                5-10                 [15]
brian_dealer         9-8                  [17]
:ok
iex(3)> Blackjack.Game.move(g)                           
(H)it/(S)tand?h
brian_op_1           A-2-7-7              [17]
brian_op_2           6-5-9                [20]
brian_op_3           2-9-8                [19]
brian_op_4           7-Q                  [17]
brian                5-10-8               [bust]
brian_dealer         9-8                  [17]
:bust
iex(4)> Blackjack.Game.draw(g)                           
DEALER HAS DRAWN
brian_op_1           A-2-7-7              [17]
brian_op_2           6-5-9                [20]
brian_op_3           2-9-8                [19]
brian_op_4           7-Q                  [17]
brian                5-10-8               [bust]
brian_dealer         9-8                  [17]
THE END
:tally
iex(5)> Blackjack.Game.next(g)                           
:start_hand
iex(6)> Blackjack.Game.deal(g)                           
INITIAL DEAL
brian_op_1           5-10                 [15]
brian_op_2           J-Q                  [20]
brian_op_3           10-2                 [12]
brian_op_4           4-K                  [14]
brian                3-4                  [7]
brian_dealer         J-A                  [21]
OPPONENTS DRAWING...
brian_op_1           5-10-8               [bust]
brian_op_2           J-Q                  [20]
brian_op_3           10-2-K               [bust]
brian_op_4           4-K-K                [bust]
brian                3-4                  [7]
brian_dealer         J-A                  [21]
:ok
iex(7)> Blackjack.Game.move(g)
(H)it/(S)tand?h
brian_op_1           5-10-8               [bust]
brian_op_2           J-Q                  [20]
brian_op_3           10-2-K               [bust]
brian_op_4           4-K-K                [bust]
brian                3-4-5                [12]
brian_dealer         J-A                  [21]
:hit
iex(9)> Blackjack.Game.move(g)
(H)it/(S)tand?h
brian_op_1           5-10-8               [bust]
brian_op_2           J-Q                  [20]
brian_op_3           10-2-K               [bust]
brian_op_4           4-K-K                [bust]
brian                3-4-5-Q              [bust]
brian_dealer         J-A                  [21]
:bust
iex(10)> Blackjack.Game.draw(g)
DEALER HAS DRAWN
brian_op_1           5-10-8               [bust]
brian_op_2           J-Q                  [20]
brian_op_3           10-2-K               [bust]
brian_op_4           4-K-K                [bust]
brian                3-4-5-Q              [bust]
brian_dealer         J-A                  [21]
THE END
:tally
```

## TODO

* end of hand comparisons

