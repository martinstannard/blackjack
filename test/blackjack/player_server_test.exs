defmodule PlayerServerTest do
  use ExUnit.Case

  alias Blackjack.PlayerServer

  setup do
    on_exit fn ->
      Enum.each PlayerServer.players, fn(player) ->
        PlayerServer.delete_player(player)
      end
    end
  end

  test "can add a player" do
    PlayerServer.add_player("martin")
    PlayerServer.add_player("hugo")

    assert Supervisor.count_children(PlayerServer).active == 2
  end

  test "can find a player" do
    PlayerServer.add_player("martin")
    player = PlayerServer.find_player("martin")
    assert is_pid(player)
    assert Blackjack.Player.name(player) == "martin"
  end

  test "can find an available player" do
    PlayerServer.add_player("martin")
    player = PlayerServer.find_available_player
    assert is_pid(player)
    assert Blackjack.Player.playing(player) == false
  end

  test "can delete a player" do
    {:ok, m} = PlayerServer.add_player("martin")
    PlayerServer.add_player("hugo")
    PlayerServer.delete_player(m)

    assert Supervisor.count_children(PlayerServer).active == 1
  end
end
