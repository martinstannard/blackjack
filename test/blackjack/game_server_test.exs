defmodule GameServerTest do
  use ExUnit.Case

  alias Blackjack.GameServer

  setup do
    on_exit fn ->
      Enum.each GameServer.games, fn(game) ->
        GameServer.delete_game(game)
      end
    end
  end

  test "can add a game" do
    GameServer.add_game("martin")
    GameServer.add_game("hugo")

    assert Supervisor.count_children(GameServer).active == 2
  end

  #test "can find a game" do
    #GameServer.add_game("martin")
    #game = GameServer.find_game("martin")
    #assert is_pid(game)
    #assert Blackjack.Game.name(game) == "martin"
  #end

  test ".delete_game deletes a game by pid" do
    {:ok, t} = GameServer.add_game("game_1")
    GameServer.add_game("game_2")
    GameServer.delete_game(t)
    assert Supervisor.count_children(GameServer).active == 1
  end
end
