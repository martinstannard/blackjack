defmodule Blackjack.GameTest do

  alias Blackjack.Game

  use ExUnit.Case, async: true

  setup do
    {:ok, game} = Game.start_link("test_game")
    {:ok, game: game}
  end

  test "can be started", %{game: game} do
    assert game
  end

end

