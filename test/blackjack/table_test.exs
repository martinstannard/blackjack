defmodule Blackjack.TableTest do

  alias Blackjack.Table

  use ExUnit.Case, async: true

  setup do
    {:ok, table} = Table.start_link("test")
    {:ok, table: table}
  end

  test "can be started", %{table: table} do
    assert table
  end

  #test "should receive deal", %{table: table} do
    #assert_received table.
  #end

end

