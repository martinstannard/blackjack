defmodule Blackjack.PlayerTest do

  use ExUnit.Case

  alias Blackjack.PlayerServer
  alias Blackjack.Player

  setup do
    on_exit fn ->
      Enum.each PlayerServer.players, fn(player) ->
        PlayerServer.delete_player(player)
      end
    end
    PlayerServer.add_player("martin")
    player = PlayerServer.find_player("martin")
    {:ok, player: player}
  end

  test "can be created", %{player: player} do
    assert is_pid(player)
  end

  test "a player has a hand", %{player: player} do
    hand = Player.hand(player)
    assert is_pid(hand)
  end

  test "a player has a name", %{player: player} do
    name = Player.name(player)
    assert name == "martin"
  end

  test "is not initially playing", %{player: player} do
    assert Player.playing(player) == false
  end

  test "sets playing to true when playing a game", %{player: player} do
    Player.play(player)
    assert Player.playing(player) == true
  end

  test "playing status can be set", %{player: player} do
    Player.play(player)
    assert Player.playing(player) == true
    Player.leave(player)
    assert Player.playing(player) == false
  end

  test "hits when < 17", %{player: player} do
    Player.draw(player, {"3", "C"})
    Player.draw(player, {"10", "H"})
    assert Player.move(player) == :hit 
  end

  test "stands when >= 17", %{player: player} do
    Player.draw(player, {"10", "C"})
    Player.draw(player, {"10", "H"})
    assert Player.move(player) == :stand 
  end

  test "bust when > 21", %{player: player} do
    Player.draw(player, {"10", "C"})
    Player.draw(player, {"10", "H"})
    Player.draw(player, {"2", "H"})
    assert Player.move(player) == :bust 
  end

  test "has 0 credits when created", %{player: player} do
    assert Player.credits(player) == 0
  end

  test "can get more credits", %{player: player} do
    Player.transact(player, 10)
    assert Player.credits(player) == 10
    Player.transact(player, -5)
    assert Player.credits(player) == 5
    Player.transact(player, 50)
    assert Player.credits(player) == 55
  end

end
