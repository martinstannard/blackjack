defmodule TableServerTest do
  use ExUnit.Case

  alias Blackjack.TableServer

  setup do
    on_exit fn ->
      Enum.each TableServer.tables, fn(table) ->
        TableServer.delete_table(table)
      end
    end
  end

  test ".add_table adds a new table" do
    TableServer.add_table("Table 1")
    TableServer.add_table("Table 2")
    assert Supervisor.count_children(TableServer).active == 2
  end

  test ".find_table finds a table by name" do
    TableServer.add_table("Table 3")
    table = TableServer.find_table("Table 3")
    assert is_pid(table)
  end

  test ".delete_table deletes a table by pid" do
    {:ok, t} = TableServer.add_table("table_1")
    TableServer.add_table("table_2")
    TableServer.delete_table(t)
    assert Supervisor.count_children(TableServer).active == 1
  end
end
