defmodule Blackjack.CounterTest do

  alias Blackjack.Counter

  use ExUnit.Case, async: true

  test "score 0 for an empty hand" do
    assert Counter.scores([]) == [0]
  end

  test "score 5 for a card with one 5 " do
    assert Counter.scores([{"5", "H"}]) == [5]
  end

  test "scores multiple cards" do
    assert Counter.scores([{"5", "H"}, {"6", "H"}]) == [11]
    assert Counter.scores([{"2", "H"}, {"6", "H"}]) == [8]
    assert Counter.scores([{"K", "H"}, {"4", "H"}]) == [14]
  end

  test "returns multiple scores for a hand with an ace" do
    assert Counter.scores([{"A", "H"}, {"6", "H"}]) == [7, 17]
  end

  test "returns multiple scores for a hands with multiple aces" do
    assert Counter.scores([{"A", "H"}, {"A", "C"}, {"6", "H"}]) == [8, 18, 28]
  end

  test "returns the best score for a hands" do
    assert Counter.best_score([{"10", "H"}, {"A", "C"}, {"9", "H"}]) == "20"
  end

  test "returns bust when all scores are > 21" do
    assert Counter.best_score([{"10", "H"}, {"5", "H"}, {"A", "C"}, {"9", "H"}]) == "bust"
  end

end

