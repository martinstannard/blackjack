defmodule Blackjack.HandTest do

  alias Blackjack.Hand

  use ExUnit.Case, async: true

  setup do
    {:ok, hand} = Hand.start_link
    {:ok, hand: hand}
  end

  test "can be started", %{hand: hand} do
    assert hand
  end

  test "initially has 0 cards", %{hand: hand} do
    assert Hand.count(hand) == 0
  end

  test "can clear hand", %{hand: hand} do
    Hand.draw(hand, {"10", "C"})
    Hand.draw(hand, {"2", "H"})
    Hand.clear(hand)
    assert Hand.count(hand) == 0
  end

  test "can draw a card", %{hand: hand} do
    Hand.draw(hand, {"2", "C"})
    assert Hand.count(hand) == 1 
  end

  test "can draw several cards", %{hand: hand} do
    Hand.draw(hand, {"2", "C"})
    Hand.draw(hand, {"3", "C"})
    Hand.draw(hand, {"4", "C"})
    assert Hand.count(hand) == 3 
  end

  test "can get scores for the hand", %{hand: hand} do
    Hand.draw(hand, {"2", "C"})
    Hand.draw(hand, {"3", "C"})
    Hand.draw(hand, {"4", "C"})
    assert Hand.scores(hand) == [9] 
  end

  test "has a printable representation", %{hand: hand} do
    Hand.draw(hand, {"2", "C"})
    Hand.draw(hand, {"3", "C"})
    Hand.draw(hand, {"4", "C"})
    assert Hand.to_string(hand) == "2-3-4 [9]" 
  end

  test ".bust? returns whether the hand is busted", %{hand: hand} do
    Hand.draw(hand, {"10", "C"})
    Hand.draw(hand, {"J", "C"})
    assert Hand.bust?(hand) == false
    Hand.draw(hand, {"2", "C"})
    assert Hand.bust?(hand) == true
  end

  test ".highest_score returns the highest possible score", %{hand: hand} do
    Hand.draw(hand, {"10", "C"})
    Hand.draw(hand, {"J", "C"})
    assert Hand.highest_score(hand) == 20
    Hand.draw(hand, {"A", "C"})
    assert Hand.highest_score(hand) == 31
  end

end
